package main

import (
	"encoding/base64"
	"encoding/csv"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"./config"
	"./console"
	"./db"
	"./stats"
	"github.com/google/uuid"
	"github.com/gorilla/securecookie"
	"github.com/julienschmidt/httprouter"
)

// CryptoParams contains all cryptographic
// parameters used for encryption and authentication
// by the program
type CryptoParams struct {
	// Encryption key for the database
	DBkey []byte

	// IV used to randomize encryption output
	DBIV []byte

	// Encryption key used to encrypt cookies
	CookieKey []byte

	// HMAC key used to ensure authenticity of cookie
	CookieAuthKey []byte
}

var (
	svstat stats.Stats

	keys *CryptoParams

	DB db.AuthDatabase
)

func main() {
	// Read encryption keys
	var err error
	keys, err = readEncryptionKeys("portal.keys")

	if err != nil {
		log.Fatal(err)
	}

	// Setup database
	DB, err = db.InitAuthDatabase("auth.db", keys.DBkey, keys.DBIV)

	if err != nil {
		log.Fatal(err)
	}

	// Setup the management console
	mc, err := console.NewManagementConsole(&DB, config.CONSOLE_PORT, config.CONSOLE_ADMIN_KEY, &svstat)

	if err != nil {
		log.Fatal(err)
	}

	// Run the console in a separate goroutine
	go mc.Run()

	// Setup the http server
	serverAddr := "127.0.0.1:" + strconv.Itoa(config.WEB_PORT)

	// Setup server routes
	router := httprouter.New()

	router.GET("/auth", authenticateUser)
	router.GET("/invite/:token", inviteActivation)

	log.Println("Starting http server on: ", serverAddr)
	// Run the http server
	log.Fatal(http.ListenAndServe(serverAddr, router))
}

func authenticateUser(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	svstat.NewAuthRequest()

	// Extract information about the request
	ip := r.Header.Get("X-Real-IP")
	location := r.Header.Get("X-Original-URI")

	log.Printf("[AUTH REQ]: IP: %s | URL: %s\n", ip, location)

	onError := func(err error) {
		// Bad request, log it!
		log.Printf("[AUTH ERR] %s", err)
		svstat.AuthenticationFailed()
		http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
	}

	// Extract cookie from request
	cookie, err := r.Cookie(config.AUTH_COOKIE)

	if err != nil {
		onError(err)
		return
	}

	encCookie := securecookie.New(keys.CookieAuthKey, keys.CookieKey)

	cookieData := make(map[string]string)

	// Decrypt cookie and extract data
	err = encCookie.Decode(config.AUTH_COOKIE, cookie.Value, &cookieData)

	if err != nil {
		onError(err)
		return
	}

	// Extract uuid from the cookie
	ID, exists := cookieData["uuid"]

	if !exists {
		onError(errors.New("Can't extract UUID from cookie"))
		return
	}

	userID, err := uuid.Parse(ID)

	if err != nil {
		onError(err)
		return
	}

	// Authenticate user against database account records
	if !DB.AuthenticateUser(userID, location, ip) {
		onError(errors.New("Can't authenticate user"))
		return
	}

	svstat.NewAuthUser()

	log.Printf("[+] User '%s' was authenticated!\n", ID)

	w.WriteHeader(http.StatusOK)
	return
}

func inviteActivation(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	svstat.NewInviteRequest()

	onError := func(err error) {
		// Bad request, log it!
		log.Printf("[INV ERR] %s", err)

		svstat.LinkActivationFailed()
		http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
	}

	// Extract the random invite token
	token := p.ByName("token")

	// Extract information about the request
	ip := r.Header.Get("X-Real-IP")

	log.Printf("[INV REQ] IP: %s | Token: %s", token, ip)

	userID := DB.CheckInviteToken(token)

	if userID == uuid.Nil {
		onError(errors.New("No user for invite token"))
		return
	}

	// The invitation is still available
	svstat.NewActivatedLink()

	// Craft the authorization cookie
	scookie := securecookie.New(keys.CookieAuthKey, keys.CookieKey)
	fields := map[string]string{
		"uuid": userID.String(),
	}

	// Embed account uuid inside the encrypted cookie
	encCookie, err := scookie.Encode(config.AUTH_COOKIE, fields)

	if err != nil {
		onError(errors.New("Can't embed UUID inside cookie"))
		return
	}

	// Create the cookie
	cookie := &http.Cookie{
		Name:    config.AUTH_COOKIE,
		Value:   encCookie,
		Domain:  config.WEBSITE_DOMAIN,
		Expires: time.Now().AddDate(0, config.AUTH_COOKIE_MONTHS, config.AUTH_COOKIE_WEEKS),
		Path:    "/private",
	}

	http.SetCookie(w, cookie)

	log.Println("[+] Invitation was validated for: ", token)

	landingPage := r.Header.Get("X-Landing-Page")
	if landingPage == "" {
		w.WriteHeader(http.StatusOK)
		return
	}

	// Redirect to landing page
	http.Redirect(w, r, landingPage, http.StatusPermanentRedirect)
}

func readEncryptionKeys(path string) (*CryptoParams, error) {
	// Open encryption file as read only
	// The file contains the encryption parameters separated by ',', encoded as base64
	kf, err := os.OpenFile(path, os.O_RDONLY, 400)

	if err != nil {
		return nil, err
	}

	defer kf.Close()

	// Interpret keyfile as a csv document
	r := csv.NewReader(kf)

	// All keys should be on the first line
	keys, err := r.Read()

	if err != nil {
		return nil, err
	}

	// Validate and copy keys in the memory
	if len(keys) != 4 {
		return nil, fmt.Errorf("Invalid number of keys: %d", len(keys))
	}

	// Decode one key at a time
	c := CryptoParams{}

	// Decode key
	log.Println("Decoding K1")
	c.DBkey, err = base64.StdEncoding.DecodeString(keys[0])
	if err != nil {
		return nil, err
	}

	// Validate key len
	if len(c.DBkey) != db.AES_256_KEYLEN {
		return nil, fmt.Errorf("Invalid key lenght for db key: %d", len(c.DBkey))
	}

	log.Println("Decoding K2")
	c.DBIV, err = base64.StdEncoding.DecodeString(keys[1])
	if err != nil {
		return nil, err
	}

	if len(c.DBIV) != db.NONCE_LEN {
		return nil, fmt.Errorf("Invalid key lenght for db IV: %d", len(c.DBIV))
	}

	log.Println("Decoding K3")
	c.CookieKey, err = base64.StdEncoding.DecodeString(keys[2])
	if err != nil {
		return nil, err
	}

	if len(c.CookieKey) != db.AES_256_KEYLEN {
		return nil, fmt.Errorf("Invalid key lenght for cookie: %d", len(c.CookieKey))
	}

	log.Println("Decoding K4")
	c.CookieAuthKey, err = base64.StdEncoding.DecodeString(keys[3])

	if err != nil {
		return nil, err
	}

	if len(c.CookieAuthKey) != db.AES_256_KEYLEN {
		return nil, fmt.Errorf("Invalid key lenght for cookie auth: %d", len(c.CookieAuthKey))
	}

	return &c, nil
}
