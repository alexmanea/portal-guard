package db

import (
	"encoding/hex"
	"encoding/json"
	"time"

	"github.com/google/uuid"
)

// InvitationURL 
type InvitationURL struct {
	// The ID corresponding with the user account
	// The UUID is also used in cookies to autheticate the user
	// The userdb uses this uuid to index accounts
	ID uuid.UUID `json:"UID"`

	// The maximum number of times the invitation link can be accessed
	// before it becomes invalid
	// Depending on the platform you're sharing the link, some URL bots
	// visit the link in order to filter unwanted content on the platform,
	// thus, the link might be accessed by another computer before the user
	// can activate it
	MaxAccess int64 `json:"max_access"`

	// The number of times the link has been accessed
	// When this filed is greater or equal with the maximum access value
	// the link becomes invalid and no access cookie is set to the client
	Accessed int64 `json:"accessed"`

	ExpirationDate time.Time `json:"expiration_date"`
}

// NewInvitationURL returns a random token
// and a InvitationURL structure with the apropiate settings
func NewInvitationURL(id uuid.UUID, maxAccess int64, expires time.Time) (string, InvitationURL) {
	// Generate a random token
	secureToken, err := GenerateRandom(64)

	if err != nil {
		return "", InvitationURL{}
	}

	// Hex encode the secure token
	token := hex.EncodeToString(secureToken)

	return token, InvitationURL{
		ID:             id,
		MaxAccess:      maxAccess,
		ExpirationDate: expires,
	}
}

// Valid returns the user uuid if the link is in a valid state:
// The link can be invalidated if:
// it expires
// the access count excedees the maximum allowed value
func (i *InvitationURL) Valid() uuid.UUID {
	// The link was accessed to many times
	if i.Accessed >= i.MaxAccess {
		return uuid.Nil
	}

	// The link expired
	if time.Now().After(i.ExpirationDate) {
		return uuid.Nil
	}

	// The link is in a valid state

	i.Accessed++

	return i.ID
}

// Marshal encodes invite url to JSON
func (i *InvitationURL) Marshal() ([]byte, error) {
	return json.MarshalIndent(i, "", "    ")
}

// Unmarshal decodes invite url from JSON
func (i *InvitationURL) Unmarshal(data []byte) error {
	return json.Unmarshal(data, i)
}
