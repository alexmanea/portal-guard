package db

// All data stored in the database will be encrypted using AEAD
// More specifically, AES-256-GCM

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"errors"
)

// AES_256_KEYLEN is the required key length needed for AES-256 encryption
const AES_256_KEYLEN = 32

// NONCE_LEN is the required length of the nonce used by the encryption algorithm
const NONCE_LEN = 16

// EncryptionLayer provides a simple interface
// for encrypting and decrypting data using AES-256-GCM encryption
// AES-256-GCM provides both confidentiality and data integrity
type EncryptionLayer struct {
	key   [AES_256_KEYLEN]byte
	nonce [NONCE_LEN]byte

	aesgcm cipher.AEAD
}

/*
	// Never use more than 2^32 random nonces with a given key because of the risk of a repeat.
	nonce := make([]byte, 12)
	if _, err := io.ReadFull(rand.Reader, nonce); err != nil {
		return EncryptionLayer{}, err
	}

	Random nonce
*/

// NewEncryptionLayer initializes the encryption algorithm
func NewEncryptionLayer(encKey, randomNonce []byte) (EncryptionLayer, error) {

	if len(encKey) != AES_256_KEYLEN {
		return EncryptionLayer{}, errors.New("Invalid key length")
	}

	if len(randomNonce) != NONCE_LEN {
		return EncryptionLayer{}, errors.New("Invalid nonce length")
	}

	e := EncryptionLayer{}

	copy(e.key[:], encKey)
	copy(e.nonce[:], randomNonce)

	// Allocate an AES-256 cipher
	block, err := aes.NewCipher(e.key[:])
	if err != nil {
		return EncryptionLayer{}, err
	}

	e.aesgcm, err = cipher.NewGCMWithNonceSize(block, NONCE_LEN)

	if err != nil {
		return EncryptionLayer{}, err
	}

	return e, nil
}

// Encrypt ciphers the data using AES-256-GCM
func (e *EncryptionLayer) Encrypt(data []byte) []byte {
	// Check if encryption algorithm was initialized
	if e.aesgcm == nil {
		return nil
	}

	return e.aesgcm.Seal(nil, e.nonce[:], data, nil)
}

// Decrypt deciphers the data using AES-256-GCM
func (e *EncryptionLayer) Decrypt(data []byte) ([]byte, error) {
	// Check if encryption algorithm was initialized
	if e.aesgcm == nil {
		return nil, errors.New("Uninitialized encryption layer")
	}

	return e.aesgcm.Open(nil, e.nonce[:], data, nil)
}

// GenerateRandom generates a secure random byte slice
func GenerateRandom(size int32) ([]byte, error) {
	data := make([]byte, size)

	// Generate a random token
	_, err := rand.Read(data)

	return data, err
}
