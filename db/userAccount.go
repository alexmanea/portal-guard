package db

import (
	"encoding/json"
	"time"
	"strings"
)

// PageStats represents the fields used
// to track user activity on the website
type PageStats struct {
	// Path to a page
	Path string `json:"path"`

	// Counter that increases with each visit of that page
	Visisted int64 `json:"visited"`
}

// Account represents the information associated
// with a registered user in order to provide access
// and activity information on the website
type Account struct {
	// The name of the person the account belongs to
	Name string `json:"name"`

	// Flag that enables the logging of user activities
	// on the website.
	// If false, the server will not log any activity about the
	// user such as: visited pages, ip addresses and timestamps
	Tracking bool `json:"track"`

	// What pages the user visited since registered
	VisitedPages []PageStats `json:"visited"`

	// List of IP addresses that the user
	// used to access the website
	Addresses []string `json:"addresses"`

	// Timestamp of last activity
	// on the website
	LastSeen time.Time `json:"last_seen"`

	// Timestamp of the date when the account
	// was created
	Created time.Time `json:"created"`
}

// NewAccount constructs an account structure
func NewAccount(name string, trackTheUser bool) Account {
	return Account{
		Name:     name,
		Tracking: trackTheUser,
		Created:  time.Now(),
	}
}

// Marshal encodes account info to JSON
func (a *Account) Marshal() ([]byte, error) {
	return json.MarshalIndent(a, "", "    ")
}

// Unmarshal decodes Account from JSON
func (a *Account) Unmarshal(data []byte) error {
	return json.Unmarshal(data, a)
}

// Visited updates the access information
// every time a user is successfully authenticated
func (a *Account) Visited(page, ip string) {
	// Don't log activity if tracking is disabled
	if !a.Tracking {
		return
	}

	// Update last activity timestamp
	a.LastSeen = time.Now()

	// Update access ip list
	newIP := true
	for _, addr := range a.Addresses {
		if addr == ip {
			newIP = false
			break
		}
	}

	if newIP {
		a.Addresses = append(a.Addresses, ip)
	}


	cleanPage := filterVisitedPages(page)

	// Ignore page
	if cleanPage == ""{
		return
	}

	// Find page and update count if visited before
	// Linear search is enough as we don't have a lot of pages
	updated := false
	for i, p := range a.VisitedPages {
		if p.Path == cleanPage {
			a.VisitedPages[i].Visisted++
			updated = true
			break
		}
	}

	if !updated {
		newPage := PageStats{
			Path:     cleanPage,
			Visisted: 1,
		}

		// Add the new page to the list
		a.VisitedPages = append(a.VisitedPages, newPage)
	}
}


// Filter pages that bloat the tracking function:
// resources as css, js files are common for most pages
// a trailing slash can make the difference, thus we get to store
// duplicate entries of the same page
// It returns a clean version of the page that the user just visited
// If the returned string is empty, the page should be ignored!
func filterVisitedPages(page string) string{
	clean := strings.TrimSuffix(page, "/")

	// Ignore js files
	if strings.Contains(clean, ".js") {
		return ""
	}

	// Ignore css files
	if strings.Contains(clean, ".css") {
		return ""
	}

	return clean
}
