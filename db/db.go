package db

import (
	"errors"
	"fmt"
	"time"

	"github.com/google/uuid"

	"github.com/boltdb/bolt"
)

// Structure used to simplify database operations
// for the authentification system:
// User account verification, creation and update
// Invite link creation and cancelation

// Users account info will be stored under a randomly generated UUID

// Store the IV in the database

const urlBucket = "invite"
const userBucket = "accounts"

// AuthDatabase provides methods
// for easy access to database TODO write better docs
type AuthDatabase struct {
	// Database object to the information db?
	authdb *bolt.DB

	enc EncryptionLayer
}

// InitAuthDatabase opens and intializes the database for transactions
func InitAuthDatabase(path string, dbKey, dbIV []byte) (AuthDatabase, error) {
	db, err := bolt.Open(path, 0600, &bolt.Options{Timeout: 5 * time.Second})

	if err != nil {
		return AuthDatabase{}, err
	}

	// Initialize buckets
	err = db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(userBucket))

		if err != nil {
			return fmt.Errorf("Can't create bucket '%s': %s", userBucket, err)
		}

		_, err = tx.CreateBucketIfNotExists([]byte(urlBucket))

		if err != nil {
			return fmt.Errorf("Can't create bucket '%s': %s", urlBucket, err)
		}

		return nil
	})

	if err != nil {
		return AuthDatabase{}, err
	}

	// Initialize encryption layer
	adb := AuthDatabase{
		authdb: db,
	}

	adb.enc, err = NewEncryptionLayer(dbKey, dbIV)

	return adb, err
}

// GetStats retrives statistics structure from the underlying database
func (adb *AuthDatabase) GetStats() bolt.Stats {
	return adb.authdb.Stats()
}

// AuthenticateUser TODO
func (adb *AuthDatabase) AuthenticateUser(userID uuid.UUID, page, ip string) bool {
	err := adb.authdb.Update(func(tx *bolt.Tx) error {
		users := tx.Bucket([]byte(userBucket))

		if users == nil {
			return fmt.Errorf("Can't open bucket '%s'", userBucket)
		}

		// Doesn't return an error. See https://github.com/google/uuid/blob/master/marshal.go#L27
		uidBytes, _ := userID.MarshalBinary()
		uuidStr := userID.String()

		// Encrypt the uuid
		encUUID := adb.enc.Encrypt(uidBytes)

		if encUUID == nil {
			return fmt.Errorf("Failed to encrypt UUID: %s", uuidStr)
		}

		// Search for encrypted uuid
		encAccount := users.Get([]byte(encUUID))

		// No account found at uuid
		if encAccount == nil {
			return fmt.Errorf("No account associated with: %s", uuidStr)
		}

		// Decrypt the account data in order to update it
		account, err := adb.enc.Decrypt(encAccount)

		if err != nil {
			return fmt.Errorf("Can't decrypt account for '%s': %s", uuidStr, err)
		}

		// Wipe account info copy from memory
		//defer memguard.WipeBytes(account)

		// User exists. Update activity information
		acc := Account{}

		err = acc.Unmarshal(account)

		if err != nil {
			return fmt.Errorf("Can't decode account for '%s': %s", uuidStr, err)
		}

		// If tracking is disabled for the account, this
		// function will do nothing
		acc.Visited(page, ip)

		updatedAccount, err := acc.Marshal()

		if err != nil {
			return fmt.Errorf("Can't encode account for '%s': %s", uuidStr, err)
		}

		// Wipe updated account info from memory
		//defer memguard.WipeBytes(updatedAccount)

		// Encrypt the account info before storing it in the database
		encUpdatedAccount := adb.enc.Encrypt(updatedAccount)

		if encUpdatedAccount == nil {
			return fmt.Errorf("Can't encrypt account for '%s'", uuidStr)
		}

		// Store the new account info
		err = users.Put(encUUID, encUpdatedAccount)

		if err != nil {
			return fmt.Errorf("Can't store account '%s': %s", uuidStr, err)
		}

		return nil
	})

	// Can't check the user
	if err != nil {
		// Log the error
		return false
	}

	return true
}

// CheckInviteToken will return the UUID of the user account
// that the invitation was for if the invitation is still available
// test to make sure that the UUID string is copied
func (adb *AuthDatabase) CheckInviteToken(token string) uuid.UUID {
	UUID := uuid.Nil

	err := adb.authdb.Update(func(tx *bolt.Tx) error {
		url := tx.Bucket([]byte(urlBucket))

		if url == nil {
			return fmt.Errorf("Can't open bucket '%s'", urlBucket)
		}

		// Encrypt the token
		encToken := adb.enc.Encrypt([]byte(token))

		if encToken == nil {
			return fmt.Errorf("Failed to encrypt token: %s", token)
		}

		// Search for encrypted token
		encInvitation := url.Get([]byte(encToken))

		// No invite link found for token
		if encInvitation == nil {
			return fmt.Errorf("No invitation for token: %s", token)
		}

		// Decrypt the invitation url in order to validate it
		invitation, err := adb.enc.Decrypt(encInvitation)

		if err != nil {
			return fmt.Errorf("Can't decrypt invitation data for '%s': %s", token, err)
		}

		// Wipe invitation info copy from memory
		//defer memguard.WipeBytes(invitation)

		// User exists. Update activity information
		inv := InvitationURL{}

		err = inv.Unmarshal(invitation)

		if err != nil {
			return fmt.Errorf("Can't decode invitation for token '%s': %s", token, err)
		}

		id := inv.Valid()

		if id == uuid.Nil {
			return fmt.Errorf("Invitation for token '%s' is invalid", token)
		}

		// Copy the uuid to the outside context
		UUID = id

		updatedInvite, err := inv.Marshal()

		if err != nil {
			return fmt.Errorf("Can't encode invitation for token '%s': %s", token, err)
		}

		// Wipe updated account info from memory
		//defer memguard.WipeBytes(updatedInvite)

		// Encrypt the account info before storing it in the database
		encUpdatedInvite := adb.enc.Encrypt(updatedInvite)

		if encUpdatedInvite == nil {
			return fmt.Errorf("Can't encrypt invitation for token '%s'", token)
		}

		// Store the invitation to disk
		err = url.Put(encToken, encUpdatedInvite)

		if err != nil {
			return fmt.Errorf("Can't store invitation for token '%s': %s", token, err)
		}

		return nil
	})

	// Can't check the link
	if err != nil {
		// Log the error
		return uuid.Nil
	}

	return UUID
}

// RegisterUser TODO
func (adb *AuthDatabase) RegisterUser(acc *Account) (uuid.UUID, error) {
	if acc == nil {
		return uuid.Nil, errors.New("Can't register a null account")
	}

	userUUID := uuid.Nil

	err := adb.authdb.Update(func(tx *bolt.Tx) error {
		users := tx.Bucket([]byte(userBucket))

		if users == nil {
			return fmt.Errorf("Can't open bucket '%s'", userBucket)
		}

		// Generate a new random UUID
		userID, err := uuid.NewRandom()

		if err != nil {
			return fmt.Errorf("Failed to generate UUID for user account '%s'", acc.Name)
		}

		// Doesn't return an error. See https://github.com/google/uuid/blob/master/marshal.go#L27
		uidBytes, _ := userID.MarshalBinary()
		uuidStr := userID.String()

		// Encrypt the uuid
		encUUID := adb.enc.Encrypt([]byte(uidBytes))

		if encUUID == nil {
			return fmt.Errorf("Failed to encrypt UUID: %s", uuidStr)
		}

		// Encode account to binary
		accData, err := acc.Marshal()

		if err != nil {
			return fmt.Errorf("Can't encode account for '%s': %s", uuidStr, err)
		}

		// Wipe account info from memory after storing it in the database
		//defer memguard.WipeBytes(accData)

		// Encrypt the account info before storing it in the database
		encUpdatedAccount := adb.enc.Encrypt(accData)

		if encUpdatedAccount == nil {
			return fmt.Errorf("Can't encrypt account for '%s'", uuidStr)
		}

		// Store the new account info
		err = users.Put(encUUID, encUpdatedAccount)

		if err != nil {
			return fmt.Errorf("Can't store account '%s': %s", uuidStr, err)
		}

		// Copy the new generated uuid for returning
		userUUID = userID

		return nil
	})

	return userUUID, err
}

// RegisterInvitation TODO
func (adb *AuthDatabase) RegisterInvitation(token string, inv *InvitationURL) error {
	if inv == nil {
		return errors.New("Can't register a null invitation")
	}

	return adb.authdb.Update(func(tx *bolt.Tx) error {
		url := tx.Bucket([]byte(urlBucket))

		if url == nil {
			return fmt.Errorf("Can't open bucket '%s'", urlBucket)
		}

		// Encrypt the token
		encToken := adb.enc.Encrypt([]byte(token))

		if encToken == nil {
			return fmt.Errorf("Failed to encrypt token: %s", token)
		}

		inviteData, err := inv.Marshal()

		if err != nil {
			return fmt.Errorf("Can't encode invitation for token '%s': %s", token, err)
		}

		// Wipe updated account info from memory
		//defer memguard.WipeBytes(inviteData)

		// Encrypt the account info before storing it in the database
		encInvite := adb.enc.Encrypt(inviteData)

		if encInvite == nil {
			return fmt.Errorf("Can't encrypt invitation for token '%s'", token)
		}

		// Store the invitation to disk
		err = url.Put(encToken, encInvite)

		if err != nil {
			return fmt.Errorf("Can't store invitation for token '%s': %s", token, err)
		}

		return nil
	})
}

// RemoveUser TODO
func (adb *AuthDatabase) RemoveUser(userID uuid.UUID) error {
	return adb.authdb.Update(func(tx *bolt.Tx) error {
		users := tx.Bucket([]byte(userBucket))

		if users == nil {
			return fmt.Errorf("Can't open bucket '%s'", userBucket)
		}

		// Doesn't return an error. See https://github.com/google/uuid/blob/master/marshal.go#L27
		uidBytes, _ := userID.MarshalBinary()
		uuidStr := userID.String()

		// Encrypt the uuid
		encUUID := adb.enc.Encrypt(uidBytes)

		if encUUID == nil {
			return fmt.Errorf("Failed to encrypt UUID: %s", uuidStr)
		}

		// Remove the account from the database
		return users.Delete(encUUID)
	})
}

// RemoveInvite TODO
func (adb *AuthDatabase) RemoveInvite(token string) error {
	return adb.authdb.Update(func(tx *bolt.Tx) error {
		url := tx.Bucket([]byte(urlBucket))

		if url == nil {
			return fmt.Errorf("Can't open bucket '%s'", urlBucket)
		}

		// Encrypt the token
		encToken := adb.enc.Encrypt([]byte(token))

		if encToken == nil {
			return fmt.Errorf("Failed to encrypt token: %s", token)
		}

		// Remove invitation from the database
		return url.Delete(encToken)
	})
}

// RemapAccount TODO
func (adb *AuthDatabase) RemapAccount(userID uuid.UUID) error {
	// Get the account info
	// Generate a new UUID
	// Remove the account entry
	// Store old account under the new UUID

	return adb.authdb.Update(func(tx *bolt.Tx) error {
		users := tx.Bucket([]byte(userBucket))

		if users == nil {
			return fmt.Errorf("Can't open bucket '%s'", userBucket)
		}

		// Doesn't return an error. See https://github.com/google/uuid/blob/master/marshal.go#L27
		uidBytes, _ := userID.MarshalBinary()
		uuidStr := userID.String()

		// Encrypt the uuid
		encUUID := adb.enc.Encrypt(uidBytes)

		if encUUID == nil {
			return fmt.Errorf("Failed to encrypt UUID: %s", uuidStr)
		}

		// Search for encrypted uuid
		encAccount := users.Get([]byte(encUUID))

		// No account found at uuid
		if encAccount == nil {
			return fmt.Errorf("No account associated with: %s", uuidStr)
		}

		// Generate new UUID
		newUserID, err := uuid.NewRandom()

		if err != nil {
			return fmt.Errorf("Failed to generate UUID: '%s'", err)
		}

		newUIDBytes, _ := newUserID.MarshalBinary()

		// Encrypt the new uuid
		newEncUUID := adb.enc.Encrypt(newUIDBytes)

		if newEncUUID == nil {
			return fmt.Errorf("Failed to encrypt UUID: %s", newUserID.String())
		}

		// Delete record
		err = users.Delete(encUUID)

		if err != nil {
			return fmt.Errorf("Can't delete account for '%s': %s", uuidStr, err)
		}

		// Store the account at a new key
		err = users.Put(newEncUUID, encAccount)

		if err != nil {
			return fmt.Errorf("Can't store account '%s': %s", uuidStr, err)
		}

		return nil
	})
}

// DumpLinks todo
func (adb *AuthDatabase) DumpLinks() ([]string, []InvitationURL, error) {
	tokens := make([]string, 0)
	invitations := make([]InvitationURL, 0)

	err := adb.authdb.View(func(tx *bolt.Tx) error {
		url := tx.Bucket([]byte(urlBucket))

		if url == nil {
			return fmt.Errorf("Can't open bucket '%s'", urlBucket)
		}

		return url.ForEach(func(encToken, encInvite []byte) error {
			// Decrypt token
			token, err := adb.enc.Decrypt(encToken)

			if err != nil {
				return fmt.Errorf("Can't decrypt token: %s", err)
			}

			// Decrypt invite
			inviteData, err := adb.enc.Decrypt(encInvite)

			if err != nil {
				return fmt.Errorf("Can't decrypt invitation data: %s", err)
			}

			// Wipe invite data from memory
			//defer memguard.WipeBytes(inviteData)

			// Decode invite
			inv := InvitationURL{}
			err = inv.Unmarshal(inviteData)

			if err != nil {
				return fmt.Errorf("Can't decode invitation: %s", err)
			}

			// Copy token and invite
			tokens = append(tokens, string(token))
			invitations = append(invitations, inv)

			return nil
		})
	})

	return tokens, invitations, err
}

// DumpUsers todo
func (adb *AuthDatabase) DumpUsers() ([]uuid.UUID, []Account, error) {
	userIDs := make([]uuid.UUID, 0)
	accounts := make([]Account, 0)

	err := adb.authdb.View(func(tx *bolt.Tx) error {
		userb := tx.Bucket([]byte(userBucket))

		if userb == nil {
			return fmt.Errorf("Can't open bucket '%s'", userBucket)
		}

		return userb.ForEach(func(userUUID, encAccount []byte) error {
			// Decrypt user uuid
			clearUUID, err := adb.enc.Decrypt(userUUID)

			if err != nil {
				return fmt.Errorf("Can't decrypt token: %s", err)
			}

			userID, err := uuid.FromBytes(clearUUID)

			if err != nil {
				return fmt.Errorf("Can't decode UUID: %s", err)
			}

			// Decrypt user account
			inviteAccount, err := adb.enc.Decrypt(encAccount)

			if err != nil {
				return fmt.Errorf("Can't decrypt invitation data: %s", err)
			}

			// Wipe account data from memory
			//defer memguard.WipeBytes(inviteAccount)

			// Decode account data into a structure-
			acc := Account{}
			err = acc.Unmarshal(inviteAccount)

			if err != nil {
				return fmt.Errorf("Can't decode account: %s", err)
			}

			// Copy uuid and account
			userIDs = append(userIDs, userID)
			accounts = append(accounts, acc)

			return nil
		})
	})

	return userIDs, accounts, err
}

// GetUserAccount whatever
func (adb *AuthDatabase) GetUserAccount(userID uuid.UUID) (Account, error) {
	userAcc := Account{}

	err := adb.authdb.View(func(tx *bolt.Tx) error {
		users := tx.Bucket([]byte(userBucket))

		if users == nil {
			return fmt.Errorf("Can't open bucket '%s'", userBucket)
		}

		// Doesn't return an error. See https://github.com/google/uuid/blob/master/marshal.go#L27
		uidBytes, _ := userID.MarshalBinary()
		uuidStr := userID.String()

		// Encrypt the uuid
		encUUID := adb.enc.Encrypt(uidBytes)

		if encUUID == nil {
			return fmt.Errorf("Failed to encrypt UUID: %s", uuidStr)
		}

		// Search for encrypted uuid
		encAccount := users.Get([]byte(encUUID))

		// No account found at uuid
		if encAccount == nil {
			return fmt.Errorf("No account associated with: %s", uuidStr)
		}

		// Decrypt the account data in order to update it
		account, err := adb.enc.Decrypt(encAccount)

		if err != nil {
			return fmt.Errorf("Can't decrypt account for '%s': %s", uuidStr, err)
		}

		// Wipe account info copy from memory
		//defer memguard.WipeBytes(account)

		// User exists. Update activity information
		acc := Account{}

		err = acc.Unmarshal(account)

		if err != nil {
			return fmt.Errorf("Can't decode account for '%s': %s", uuidStr, err)
		}

		// Copy account structure to the outside context
		userAcc = acc

		return nil
	})

	return userAcc, err
}

// GetInviteInfo whatever
func (adb *AuthDatabase) GetInviteInfo(userID uuid.UUID) (string, InvitationURL, error) {
	token := ""
	invitations := InvitationURL{}

	err := adb.authdb.View(func(tx *bolt.Tx) error {
		url := tx.Bucket([]byte(urlBucket))

		if url == nil {
			return fmt.Errorf("Can't open bucket '%s'", urlBucket)
		}

		return url.ForEach(func(encToken, encInvite []byte) error {
			// Decrypt invite
			inviteData, err := adb.enc.Decrypt(encInvite)

			if err != nil {
				return fmt.Errorf("Can't decrypt invitation data: %s", err)
			}

			// Wipe invite data from memory
			//defer memguard.WipeBytes(inviteData)

			// Decode invite
			inv := InvitationURL{}
			err = inv.Unmarshal(inviteData)

			if err != nil {
				return fmt.Errorf("Can't decode invitation: %s", err)
			}

			if inv.ID == userID {
				// Decrypt token
				tk, err := adb.enc.Decrypt(encToken)

				if err != nil {
					return fmt.Errorf("Can't decrypt token: %s", err)
				}

				// Copy token and invite
				token = string(tk)
				invitations = inv
			}

			return nil
		})
	})

	// No invitation found for UUID
	if token == "" {
		return "", InvitationURL{}, fmt.Errorf("No invitation found for %s", userID.String())
	}

	return token, invitations, err
}

// SetTracking whatever
func (adb *AuthDatabase) SetTracking(userID uuid.UUID, tracking bool) error {
	return adb.authdb.Update(func(tx *bolt.Tx) error {
		users := tx.Bucket([]byte(userBucket))

		if users == nil {
			return fmt.Errorf("Can't open bucket '%s'", userBucket)
		}

		// Doesn't return an error. See https://github.com/google/uuid/blob/master/marshal.go#L27
		uidBytes, _ := userID.MarshalBinary()
		uuidStr := userID.String()

		// Encrypt the uuid
		encUUID := adb.enc.Encrypt(uidBytes)

		if encUUID == nil {
			return fmt.Errorf("Failed to encrypt UUID: %s", uuidStr)
		}

		// Search for encrypted uuid
		encAccount := users.Get([]byte(encUUID))

		// No account found at uuid
		if encAccount == nil {
			return fmt.Errorf("No account associated with: %s", uuidStr)
		}

		// Decrypt the account data in order to update it
		account, err := adb.enc.Decrypt(encAccount)

		if err != nil {
			return fmt.Errorf("Can't decrypt account for '%s': %s", uuidStr, err)
		}

		// Wipe account info copy from memory
		//defer memguard.WipeBytes(account)

		// User exists. Update activity information
		acc := Account{}

		err = acc.Unmarshal(account)

		if err != nil {
			return fmt.Errorf("Can't decode account for '%s': %s", uuidStr, err)
		}

		// Set tracking flag to the provided value
		acc.Tracking = tracking

		updatedAccount, err := acc.Marshal()

		if err != nil {
			return fmt.Errorf("Can't encode account for '%s': %s", uuidStr, err)
		}

		// Wipe updated account info from memory
		//defer memguard.WipeBytes(updatedAccount)

		// Encrypt the account info before storing it in the database
		encUpdatedAccount := adb.enc.Encrypt(updatedAccount)

		if encUpdatedAccount == nil {
			return fmt.Errorf("Can't encrypt account for '%s'", uuidStr)
		}

		// Store the new account info
		err = users.Put(encUUID, encUpdatedAccount)

		if err != nil {
			return fmt.Errorf("Can't store account '%s': %s", uuidStr, err)
		}

		return nil
	})
}

// GetDatabaseStats returns bolt database stats
func (adb *AuthDatabase) GetDatabaseStats() bolt.Stats {
	return adb.authdb.Stats()
}
