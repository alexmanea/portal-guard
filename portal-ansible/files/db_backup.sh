#!/bin/sh

# Location of the file where the backup will be stored
path="{{ portal_db_backup }}/bkp-$(date +"%Y-%d-%T").db"

echo "[+] Creating backup at: $path"

cp "{{ portal_path }}/auth.db" "$path"

# Ensure proper file permisions for the backup
echo "[i] Changing file permisions..."
chown root "$path"
chgrp root "$path"
chmod 0440 "$path"
