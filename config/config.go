package config

import "time"

// The auth binary can be configured from this file

// Key used to encrypt the databases on disk
//
// NEVER store sensitive data like keys unencrypted on the disk!
// They can easily be recovered and rarely deleted on modern file systems
// WHAT I RECOMMAND :
// In linux you can mount a portion of your ram using ramfs type
// NOTICE! Use ramfs over tmpfs!!
// ramfs guarantees that the data will be stored in ram and never swapped to the disk
// if the OS runs out of memory
// /run/* type is tmpfs, thus it should be avoided because the keys can be swapped to disk

// AES_256 needs a a key of 64 bytes
const DB_KEY_SIZE = 64

// AES_256 needs a a key of 64 bytes
const COOKIE_KEY_SIZE = 64

// SSH Key used to login on the management console
const CONSOLE_ADMIN_KEY = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDZXWEb7/yX2Ut8/bx+tN3WaxU86i2rWcyNtwjo6k3z0J12T6+w4KQpcjxEo2M/e1VataKNYVh7Qu8WL14/Swv4Nw5K2/FUDyL+SMhctTSnrGHtG2hOdwk5itnZkrNloVSz1Bw40hkWpX01GGzr6v7uhAFRvZDiGlu5YhDIzQabeYuf1k51tSujthLi5t63v0BruQg7VLsSrRUlzR1kxnEamaVDkcrt4EOctruVBRmyaqPqmWv1mZhkY7XaV2vPQx0Xf3Y1BZXEZaNTZ1WV9vjrGmuARI5Svww0GEnKGOrCAHct+y7PiHJqcL+bEiZSxUVWxjDPxXnoM3ntVb7IvF9DYGHysEakVAkClx6Vz+W1T7ugO/krlcUj5LvhfyAemrH7gtTVgY+qbkwriFYbriaMlTIdas8C/uiIxfXL4Nx+OZfqwL7kY/jT6XgtXsONHfRgi9X8tfJghvr9UoVhLdvVjyBOzX36O86MXJJjZpu9fRb7GATkLdFooJt5ziMryYU="

// Private ssh key that identifies the console
const CONSOLE_HOST_KEY = `-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABlwAAAAdzc2gtcn
NhAAAAAwEAAQAAAYEAwMer7QEHencmQxkhjhaJiVh8sww9ayE3BnWYD9z5tniPZJNYJAvx
9q576F6TQE7KCNqV0zEt4ptmuMsxPDQlahhCW0wmk/CIiFP3wh3SntL0XpR5JcLrNgxM98
XA8j2mWR6Baz+bhLJAmGcB892Fsl6zU0jDeaTPbKc7WGc7iwYy8rAfb2PprJigYMugLr88
9Paqs6Ts0dmFT1iz3CaPraWKaH9NkLHbrFntin380ps8WnER/8CkcavjMK5KRm4FS9n1Mi
nAJ1GvTvwt7ZrUbR4Zl1Vi0Om2Ke3Yk1pyViWa66jvn9cPhQvuTAmkuNmnhtejvVSj/kkm
ihSeLhBq/VmfrkMKpMKgWYUKUrl3EUmg4irK8LriTQ+PH1BiEKhsTDeFq9G4lTo7rxParY
NtCYLV+J/YH424p7ytJASzoEkGHVeDJjaK9VERNUgPNXpFcYtU9Ou8WJ4UBPWDUHeUa4US
u6FZg4H+CopWep1o0YkGEx5D1nMW0BUz5cNcAq5TAAAFiHsIajR7CGo0AAAAB3NzaC1yc2
EAAAGBAMDHq+0BB3p3JkMZIY4WiYlYfLMMPWshNwZ1mA/c+bZ4j2STWCQL8faue+hek0BO
ygjaldMxLeKbZrjLMTw0JWoYQltMJpPwiIhT98Id0p7S9F6UeSXC6zYMTPfFwPI9plkegW
s/m4SyQJhnAfPdhbJes1NIw3mkz2ynO1hnO4sGMvKwH29j6ayYoGDLoC6/PPT2qrOk7NHZ
hU9Ys9wmj62limh/TZCx26xZ7Yp9/NKbPFpxEf/ApHGr4zCuSkZuBUvZ9TIpwCdRr078Le
2a1G0eGZdVYtDptint2JNaclYlmuuo75/XD4UL7kwJpLjZp4bXo71Uo/5JJooUni4Qav1Z
n65DCqTCoFmFClK5dxFJoOIqyvC64k0Pjx9QYhCobEw3havRuJU6O68T2q2DbQmC1fif2B
+NuKe8rSQEs6BJBh1XgyY2ivVRETVIDzV6RXGLVPTrvFieFAT1g1B3lGuFEruhWYOB/gqK
VnqdaNGJBhMeQ9ZzFtAVM+XDXAKuUwAAAAMBAAEAAAGAG1v7HW649ft09a7aYUT3jgN95x
dEnDChk4wa4Z113xyrFRYIO01qsJzAvxC1CzpjV/BVtRxUq8VsRauouCeaa703padgDjzj
050/G1CKNLoxrj6aFu7b3NbQEZYCNR7kvd8jdOoZEzMNEdQEpex5bgAcqSQ5gckPGxd8x6
YXADsH/gNa4r8mSbV7hWFAwYGokAd14Hr2lS2uyr0HAaaDW5yT0RLIGlHzoDk/ChmpSOZ5
IUjMyiQ38l33p1AFAIvae27Mb3fwmsLNgIFatdeqoHZoFx72SFgVExZqP2XAjv2Kf9Ck69
pkhW0jAiY8uaqpcuqY9L89RV0LPed9gAi1Vb57OLi1VplDPmuGQrcnh6NhWIrqcTMwwgCC
yuOvz2ONM4OWJBi9bFqCI+XVqKUM1QzwkdllCqqckvMS2ayTmy6UBWmuSUrBdu2VkPbJz2
orwM5L+cK5TldsluCN7ePmAvfGQ/rzznMjeGgDGGMpJrj/AqbJSwdaC8vYgcPcgxt5AAAA
wQC8nCtdfILGez6s/oqs0Vh5GvbgvTcWQE3wAIXq4hK/CdgwA3lN8TpkAEcUrsMXOl9zcL
DU4zEOQEzoF2lbnl3cJn1z4TNDzE1mQWkWZPSRlfhiFhyHZCDWnBPBWtc7v6rSiKnPJbvJ
2EPQBEmEYuAFbRBv9ETQkSuaqzhF2+n9bdnMWdGXKso0jrQe8+5X3lC8M0rakJXJ2wbOVs
yc/iR65g3HTbmZ6emMk1qL7iTPB34dxwI/IblxFYFBy1ZbLVwAAADBAOnLSOOWBhQIYj4u
wgmbqf1Il1CBZSCoVsCJ4o9U+mgG4bahPlIKiy4wGHAggc63Rr2r5U39s7/jSaCudKIVx0
XQK2I5DeC0xXc3H+IzigvQ0D5/MWPdOFy2zC06Q0WsksnoedSkumjo0awLvVJTo5GHciqN
F2/DEOZxYdje/Oo0p2ZVAnK82efSc3wd06kill6ShgLIafne4joSXIKErzF2GmzLPqYXn8
hrYTnSn5QNb9LVi3yGEKLu5s0Z4k+mVwAAAMEA0xcgZT8WuqXpa6mwgJj03yrYaXqxN/c9
P/QYNKDsv6816IJ04A1odmkajQU22DSfjU4Y5YWeV+5yj4p0iPkkoGT1YO59RD53cuTWlp
PKiiPEOmlgrsIH7YEeAuPsx6S4XlA2NWS26T592GzkV0lJ3XECkZqrN2JONEx7sALAKJn+
N1yt7HLfVEHbccIWmwNmqMiCibFl/iw/BfMe65/Ut1NjU+UOabvlcNfgoG5DbAyXHTJYPi
n80Gyr5kFtZaJlAAAADnF1YXJrQG5ldXRyaW5vAQIDBA==
-----END OPENSSH PRIVATE KEY-----`

// The name of the cookie that will be used for authentication
const AUTH_COOKIE = "ReHob0AM"

// AUTH_COOKIE_MONTHS + AUTH_COOKIE_WEEKS = Cookie lifetime until it expires
// Number of months until the cookie expires
const AUTH_COOKIE_MONTHS = 6

// Number of weeks until the cookie expires
const AUTH_COOKIE_WEEKS = 0

// The port that the server will listen for http connections
const WEB_PORT = 3310

// The port that the server will listen for ssh connections
const CONSOLE_PORT = 8999

// Website domain used to auto-generate an invitation link
const WEBSITE_DOMAIN = "alexmanea.site"

// Website path proxied to the authenticator
const INVITE_PAGE = "/invite"

// Maximum time for a console session until automatically disconnected
const CONSOLE_SESSION_DEADLINE = 15 * time.Minute

// Maximum time since no activity registed on the console until automatically disconnected
const CONSOLE_IDLE_TIMEOUT = 15 * time.Minute
