package console

import (
	"errors"
	"fmt"
	"io"
	"log"

	"golang.org/x/crypto/ssh/terminal"

	"github.com/google/uuid"

	"../config"
	"../db"
	"github.com/spf13/cobra"
)

/// Handlers
func (mc *ManagementConsole) newCmdHandler(cmd *cobra.Command, args []string) {
	if mc.console == nil {
		log.Println("Console is nil")
		return
	}

	if userFlag {
		acc, err := RegisterUserMenu(mc.console)

		if err != nil {
			displayError(mc.console, err)
			return
		}

		// Store the information in the database
		userID, err := mc.database.RegisterUser(acc)

		if err != nil {
			displayError(mc.console, err)
			return
		}

		fmt.Fprintf(mc.console, "[+] Registered user %s at %s\n", acc.Name, userID.String())
		return
	}

	if inviteFlag {
		token, invite, err := RegisterInviteURL(mc.console)

		if err != nil {
			displayError(mc.console, err)
			return
		}

		err = mc.database.RegisterInvitation(token, invite)

		if err != nil {
			displayError(mc.console, err)
			return
		}

		fmt.Fprintf(mc.console, "[+] Registered invitation for %s\n", invite.ID.String())
		fmt.Fprintf(mc.console, "[i] Token: '%s'\n", token)
		fmt.Fprintf(mc.console, "[i] Invite link: %s\n", CreateInvitationLink(token))

		return
	}
}

func (mc *ManagementConsole) rmCmdHandler(cmd *cobra.Command, args []string) {
	if mc.console == nil {
		return
	}

	userID, err := uuid.Parse(args[0])

	if err != nil {
		displayError(mc.console, err)
		return
	}

	if accessFlag {
		// Get account to display a summary of the account
		acc, err := mc.database.GetUserAccount(userID)

		if err != nil {
			displayError(mc.console, err)
			return
		}

		displayAccountSummary(mc.console, userID, &acc)

		doit, err := askForConfirmation(mc.console, fmt.Sprintf("revoke access to user account '%s'", userID.String()))

		if err != nil {
			displayError(mc.console, err)
			return
		}

		if !doit {
			fmt.Fprintln(mc.console, "[+] Action was canceled")
			return
		}

		err = mc.database.RemapAccount(userID)

		if err != nil {
			displayError(mc.console, err)
			return
		}

		fmt.Fprintf(mc.console, "[+] Removed access for user '%s'\n", userID.String())
	}

	if userFlag {
		// Get account to display a summary of the account
		acc, err := mc.database.GetUserAccount(userID)

		if err != nil {
			displayError(mc.console, err)
			return
		}

		displayAccountSummary(mc.console, userID, &acc)

		// Make sure the user doesn't change his mind
		doit, err := askForConfirmation(mc.console, fmt.Sprintf("delete user account '%s'", userID.String()))

		if err != nil {
			displayError(mc.console, err)
			return
		}

		if !doit {
			fmt.Fprintln(mc.console, "[+] Action was canceled")
			return
		}

		// Delete user account
		err = mc.database.RemoveUser(userID)

		if err != nil {
			displayError(mc.console, err)
			return
		}

		fmt.Fprintf(mc.console, "[+] Removed user account %s\n", userID.String())

		return
	}

	if inviteFlag {
		// Get token to display account summary
		token, inv, err := mc.database.GetInviteInfo(userID)

		displayInvitationSummary(mc.console, token, &inv)

		// Ask for confirmation
		doit, err := askForConfirmation(mc.console, fmt.Sprintf("delete invitation '%s'", token))

		if err != nil {
			displayError(mc.console, err)
			return
		}

		if !doit {
			fmt.Fprintln(mc.console, "[+] Action was canceled")
			return
		}

		// Delete invitation
		err = mc.database.RemoveInvite(token)

		if err != nil {
			displayError(mc.console, err)
			return
		}

		fmt.Fprintf(mc.console, "[+] Removed invitation with token '%s'\n", token)

		return
	}
}

func (mc *ManagementConsole) genInviteURLCmdHandler(cmd *cobra.Command, args []string) {
	if mc.console == nil {
		return
	}

	inviteLink := CreateInvitationLink(args[0])

	fmt.Fprintln(mc.console, "[+] Invite URL: ", inviteLink)
}

func (mc *ManagementConsole) trackCmdHandler(cmd *cobra.Command, args []string) {
	if mc.console == nil {
		return
	}

	userID, err := uuid.Parse(args[0])

	if err != nil {
		displayError(mc.console, err)
		return
	}

	err = mc.database.SetTracking(userID, !disableTrackingFlag)

	action := "enable"

	if disableTrackingFlag {
		action = "disable"
	}

	if err != nil {
		displayError(mc.console, err)
		return
	}

	fmt.Fprintf(mc.console, "[+] Tracking was %sd for account '%s'\n", action, userID.String())
}

func (mc *ManagementConsole) showCmdHandler(cmd *cobra.Command, args []string) {
	if mc.console == nil {
		return
	}

	//fmt.Fprintf(mc.console, "UID: %s | I: %t | U: %t\n", uuidFlag, inviteFlag, userFlag)

	// Show account info for one user
	if uuidFlag != "" {
		userID, err := uuid.Parse(uuidFlag)

		if err != nil {
			displayError(mc.console, err)
			return
		}

		acc, err := mc.database.GetUserAccount(userID)

		if err != nil {
			displayError(mc.console, err)
			return
		}

		// Ignore error. Maybe log it in the activity log
		_ = displayAccountInfo(mc.console, userID, &acc)

		return
	}

	// Display all invitations
	if inviteFlag {
		tokens, invites, err := mc.database.DumpLinks()

		if err != nil {
			displayError(mc.console, fmt.Errorf("Can't dump invite database: %s", err))
			return
		}

		if len(tokens) != len(invites) {
			displayError(mc.console, fmt.Errorf("Mismatch between tokens and invite links: %d vs %d(TK/INV)", len(tokens), len(invites)))
			return
		}

		if len(tokens) == 0 {
			fmt.Fprintln(mc.console, " [-] No invitations found in the database!")
			return
		}

		// Display invitations
		for index := range tokens {
			if fullDisplayFlag {
				err = displayInvitationInfo(mc.console, tokens[index], &invites[index])
			} else {
				err = displayInvitationSummary(mc.console, tokens[index], &invites[index])
			}

			// Can't write data to the active session. Maybe disconnect
			if err != nil {
				break
			}
		}

		fmt.Fprintf(mc.console, "\n [+] Total invitations: %d\n", len(tokens))
	}

	// Display all user accounts
	if userFlag {
		ids, accounts, err := mc.database.DumpUsers()

		if err != nil {
			displayError(mc.console, fmt.Errorf("Can't dump invite database: %s", err))
			return
		}

		if len(ids) != len(accounts) {
			displayError(mc.console, fmt.Errorf("Mismatch between user IDs and accounts: %d vs %d(ID/ACC)", len(ids), len(accounts)))
			return
		}

		if len(ids) == 0 {
			fmt.Fprintln(mc.console, " [-] No users found in the database!")
			return
		}

		// Display accounts
		for index := range ids {
			if fullDisplayFlag {
				err = displayAccountInfo(mc.console, ids[index], &accounts[index])
			} else {
				err = displayAccountSummary(mc.console, ids[index], &accounts[index])
			}

			// Can't write data to the active session
			if err != nil {
				break
			}
		}

		fmt.Fprintf(mc.console, "\n [+] Total accounts: %d\n", len(ids))
		return
	}

	if statsFlag {
		fmt.Fprintf(mc.console, " -- Usage statistics --\n\n")

		// Authentication stats
		fmt.Fprintf(mc.console, " Authentication requests: %d\n Authenticated users: %d\n Failed authentication: %d\n\n",
			mc.programStats.AuthRequests, mc.programStats.AuthUsers, mc.programStats.AuthFailed)

		// Invitation stats
		fmt.Fprintf(mc.console, " Invitation requests: %d\n Invitation activated: %d\n Failed invitation activation: %d\n\n",
			mc.programStats.InviteRequests, mc.programStats.ActivatedLinks, mc.programStats.FailedLinkActivation)

		// Management console stats
		fmt.Fprintf(mc.console, " Console connections: %d\n Console login: %d\n Console failed login: %d\n Console last activity: %s\n\n",
			mc.programStats.ConsoleConnections, mc.programStats.ConsoleLogin, mc.programStats.ConsoleLoginFail, mc.programStats.ConsoleLastLogin)

		// Database stats
		dbStat := mc.database.GetStats()

		fmt.Fprintf(mc.console, " Database read transacttions: %d\n Database write transactions: %d\n Database cursors created: %d\n\n",
			dbStat.TxN, dbStat.TxStats.Write, dbStat.TxStats.CursorCount)
		return
	}
}

/// Helper functions
func displayInvitationSummary(stream io.Writer, token string, inv *db.InvitationURL) error {
	if inv == nil {
		return nil
	}

	_, err := fmt.Fprintf(stream, " Token: %s | UUID: %s | Clicks: %d | Expires: %s\n\n", token, inv.ID.String(), inv.Accessed, inv.ExpirationDate)
	return err
}

func displayAccountSummary(stream io.Writer, userID uuid.UUID, acc *db.Account) error {
	if acc == nil {
		return nil
	}

	_, err := fmt.Fprintf(stream, " UID: %s | Name: %s | Created: %s | Tracking: %t\n\n", userID.String(), acc.Name, acc.Created.String(), acc.Tracking)
	return err
}

func displayInvitationInfo(stream io.Writer, token string, inv *db.InvitationURL) error {
	if inv == nil {
		return nil
	}

	_, err := fmt.Fprintf(stream, " --- Token: %s ---\n  [-] For user: %s\n  [-] Max clicks: %d\n  [-] Clicks: %d\n  [-] Expires at: %s\n\n",
		token, inv.ID.String(), inv.MaxAccess, inv.Accessed, inv.ExpirationDate)

	return err
}

func displayAccountInfo(stream io.Writer, userID uuid.UUID, acc *db.Account) error {
	if acc == nil {
		return nil
	}

	ipAddresses := ""
	for _, ip := range acc.Addresses {
		ipAddresses += fmt.Sprintln("  ", ip)
	}

	pages := ""
	for _, page := range acc.VisitedPages {
		pages += fmt.Sprintf("  Count: %d => %s\n", page.Visisted, page.Path)
	}

	_, err := fmt.Fprintf(stream, " --- UUID: %s ---\n  [-] Account Name: %s\n  [-] Created: %s\n  [-] Tracking: %t\n  [-] Addresses: \n%s  [-] Visited pages: \n%s  [-] Last seen: %s\n\n",
		userID.String(), acc.Name, acc.Created.String(), acc.Tracking, ipAddresses, pages, acc.LastSeen.String())

	return err
}

func displayError(stream io.Writer, err error) {
	fmt.Fprintf(stream, "[X] %s\n", err)
}

// askForConfirmation confirms a user action
// The action string completes the question 'Are you sure you want to ...?'
func askForConfirmation(term *terminal.Terminal, action string) (bool, error) {
	if term == nil {
		return false, errors.New("Invalid terminal")
	}

	fmt.Fprintf(term, "[!] Are you sure you want to %s?(y/n): ", action)

	// Remove terminal prompt
	// When a promt is set, at the end of the question above
	// the prompt is appended, confising the user
	term.SetPrompt("")

	confirm, err := term.ReadLine()

	term.SetPrompt("~: ")

	if err != nil {
		return false, err
	}

	return confirm == "y", nil
}

// CreateInvitationLink constructs a URL of the invitation page given an invite token
func CreateInvitationLink(token string) string {
	return fmt.Sprintf("https://%s%s/%s", config.WEBSITE_DOMAIN, config.INVITE_PAGE, token)
}
