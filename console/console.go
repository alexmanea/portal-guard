package console

import (
	"errors"
	"fmt"
	"io"
	"log"
	"strconv"
	"time"

	"github.com/google/uuid"
	"github.com/spf13/cobra"

	"../config"
	"../db"
	"../stats"
	"github.com/gliderlabs/ssh"
	xssh "golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/terminal"
)

/*
	In order to securly manage the.. Write code not documentation! :)

*/
// https://github.com/gliderlabs/ssh
// cobra for cli

// ManagementConsole TODO
type ManagementConsole struct {
	database *db.AuthDatabase

	consoleServer *ssh.Server

	console *terminal.Terminal

	consoleKey *ssh.PublicKey

	rootCmd *cobra.Command

	programStats *stats.Stats
}

// NewManagementConsole TODO
func NewManagementConsole(authdb *db.AuthDatabase, port uint16, authKey string, st *stats.Stats) (*ManagementConsole, error) {
	if authdb == nil {
		return nil, errors.New("No database provided")
	}

	// Parse ssh public key
	accessKey, _, _, _, err := ssh.ParseAuthorizedKey([]byte(authKey))

	if err != nil {
		return nil, err
	}

	mc := ManagementConsole{
		database:     authdb,
		consoleKey:   &accessKey,
		programStats: st,
	}

	// Configure ssh server
	sshAddr := "127.0.0.1:" + strconv.Itoa(int(port))

	// Parse console's private key
	hostKey, err := xssh.ParsePrivateKey([]byte(config.CONSOLE_HOST_KEY))

	if err != nil {
		return nil, err
	}

	mc.consoleServer = &ssh.Server{
		Addr:        sshAddr,
		MaxTimeout:  config.CONSOLE_SESSION_DEADLINE,
		IdleTimeout: config.CONSOLE_IDLE_TIMEOUT,
		Handler:     mc.adminShell,
		PublicKeyHandler: func(ctx ssh.Context, key ssh.PublicKey) bool {
			mc.programStats.NewConsoleConnections()

			allowed := ssh.KeysEqual(key, accessKey)

			if !allowed {
				mc.programStats.ConsoleFailedLogin()
			}

			return allowed
		},
	}

	// Register host key for the ssh server
	mc.consoleServer.AddHostKey(hostKey)

	mc.rootCmd = &cobra.Command{
		Hidden:           true,
		SilenceUsage:     true,
		TraverseChildren: true,
	}

	// Init cli
	mc.initCLICommands()

	return &mc, nil
}

func (mc *ManagementConsole) initCLICommands() {
	if mc.rootCmd == nil {
		log.Println("Can't init cli: root cmd is nil")
		return
	}

	// Set global flags
	mc.rootCmd.PersistentFlags().BoolVarP(&userFlag, "user", "u", false, "Refer to an user accounts")
	mc.rootCmd.PersistentFlags().BoolVarP(&inviteFlag, "invite", "i", false, "Refer to an invitation links")

	// Set local flags
	track.Flags().BoolVarP(&disableTrackingFlag, "off", "", false, "Disable tracking for a given account")

	rm.Flags().BoolVarP(&accessFlag, "access", "", false, "Invalidate access cookie for a user account")

	show.Flags().StringVarP(&uuidFlag, "account", "a", "", "User account unique ID")
	show.Flags().BoolVarP(&statsFlag, "stats", "", false, "Display program and database statistics")
	show.Flags().BoolVarP(&fullDisplayFlag, "full", "f", false, "Display all available information about accounts/invitations")

	// Set command handlers
	new.Run = mc.newCmdHandler
	rm.Run = mc.rmCmdHandler
	genInviteURL.Run = mc.genInviteURLCmdHandler
	track.Run = mc.trackCmdHandler
	show.Run = mc.showCmdHandler

	// Create command hierarchy
	mc.rootCmd.AddCommand(new)
	mc.rootCmd.AddCommand(rm)
	mc.rootCmd.AddCommand(show)
	mc.rootCmd.AddCommand(track)
	mc.rootCmd.AddCommand(genInviteURL)
}

// Run calls ListenAndServe on the ssh server
func (mc *ManagementConsole) Run() {
	// Make sure the ssh server was initialized
	if mc.consoleServer == nil {
		log.Fatal("Uninitialized ssh server")
		return
	}

	log.Println("Management Console can be accessed at: ", mc.consoleServer.Addr)
	log.Println(" ~ Console max session time: ", mc.consoleServer.MaxTimeout)
	log.Println(" ~ Console idle timeout: ", mc.consoleServer.IdleTimeout)

	log.Fatal(mc.consoleServer.ListenAndServe())
}

func (mc *ManagementConsole) adminShell(s ssh.Session) {
	defer s.Close()

	// Update stats
	mc.programStats.NewLogin()
	mc.programStats.NewConsoleLogin()

	// Create a terminal for the current session
	term := terminal.NewTerminal(s, "~: ")
	mc.console = term

	// Print a lil banner
	io.WriteString(term, banner)

	// Print timeouts
	fmt.Fprintln(term, " ~ Console max session time: ", mc.consoleServer.MaxTimeout)
	fmt.Fprintln(term, " ~ Console idle timeout: ", mc.consoleServer.IdleTimeout)

	// Set output to our ssh session
	mc.rootCmd.SetOut(s)

	// Command aliases to save some precious keystrokes
	aliases := map[string]string{
		"nu": "new --user",
		"ni": "new --invite",

		"ru": "rm --user",
		"ri": "rm --invite",
		"ra": "rm --access",

		"sa": "show --account",
		"si": "show --invite",
		"su": "show --user",
		"ss": "show --stats",

		"t":  "track",
		"to": "track --off",

		"giu": "gen-invite-url",
	}

	for {
		// Read command
		cmd, err := term.ReadLine()

		if err != nil {
			log.Print(formatError("CONSOLE", err))
			break
		}

		if cmd == "exit" {
			io.WriteString(term, " See ya!\n")
			log.Print(formatError("CONSOLE", errors.New("Admin closed the session")))
			break
		}

		if cmd == "aliases" {
			aliasDump := " -- Console Aliases --\n"

			for alias, command := range aliases {
				aliasDump += fmt.Sprintf(" %-4s => %s\n", alias, command)
			}

			io.WriteString(term, aliasDump)
			continue
		}

		// Process the command
		err = processCommand(cmd, &aliases, mc.rootCmd)

		if err != nil {
			io.WriteString(s, formatError("PARSING", err))
		}

		// Reset flags to default values so that the next
		// command can be parsed properly
		resetFlags()
	}

	// Mark terminal as invalid
	mc.console = nil
}

func resetFlags() {
	userFlag = false
	inviteFlag = false
	accessFlag = false
	statsFlag = false
	disableTrackingFlag = false
	fullDisplayFlag = false

	uuidFlag = ""
}

func formatError(what string, err error) string {
	return fmt.Sprintf("[%s ERROR]: %s\n", what, err)
}

// RegisterUserMenu todo
func RegisterUserMenu(term *terminal.Terminal) (*db.Account, error) {
	if term == nil {
		return nil, errors.New("Invalid terminal")
	}

	io.WriteString(term, "-- Register User --\n")
	io.WriteString(term, " User name: ")

	name, err := term.ReadLine()

	if err != nil {
		return nil, err
	}

	io.WriteString(term, " Do you want to track the user activity?(y/n): ")

	// Don't convert to lower. Just compare the string
	trackOption, err := term.ReadLine()

	if err != nil {
		return nil, err
	}

	// If the user enters any string other than 'y', we default to no tracking
	track := trackOption == "y"

	acc := db.NewAccount(name, track)

	return &acc, nil
}

// RegisterInviteURL todo
func RegisterInviteURL(term *terminal.Terminal) (string, *db.InvitationURL, error) {
	if term == nil {
		return "", nil, errors.New("Invalid terminal")
	}

	io.WriteString(term, "-- Register Invitation --\n")
	io.WriteString(term, " User UUID: ")

	userIDStr, err := term.ReadLine()

	if err != nil {
		return "", nil, err
	}

	// uuid from string
	userID, err := uuid.Parse(userIDStr)

	if err != nil {
		return "", nil, fmt.Errorf("Can't parse UUID: %s", err)
	}

	io.WriteString(term, " Maximum number of visits(int): ")
	maxAccessStr, err := term.ReadLine()

	if err != nil {
		return "", nil, err
	}

	// Convert from string to int64
	maxAccess, err := strconv.ParseInt(maxAccessStr, 10, 64)

	if err != nil {
		return "", nil, fmt.Errorf("Can't parse int: %s", err)
	}

	io.WriteString(term, " How many days should the link be valid(int): ")
	daysStr, err := term.ReadLine()

	if err != nil {
		return "", nil, err
	}

	// Convert from string to int64
	days, err := strconv.ParseInt(daysStr, 10, 32)

	if err != nil {
		return "", nil, fmt.Errorf("Can't parse int: %s", err)
	}

	expires := time.Now().AddDate(0, 0, int(days))

	token, inv := db.NewInvitationURL(userID, maxAccess, expires)

	return token, &inv, nil
}
