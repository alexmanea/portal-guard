package console

import (
	"errors"
	"fmt"
	"strings"

	"github.com/google/uuid"

	"github.com/spf13/cobra"
)

const banner = `     _________
    / ======= \
   /___________\
  | ___________ |
  | | >       | |
  | |         | |
  | |_________| |__________________________
  \=____________/      Welcome to the      )
  / """"""""""" \     Management Console  /
 / ::::::::::::: \                    =D-'
(_________________)
===================
`

// Flags
var userFlag bool
var inviteFlag bool
var accessFlag bool
var statsFlag bool
var fullDisplayFlag bool

var disableTrackingFlag bool

var uuidFlag string

// CLI Commands
var new = &cobra.Command{
	Use:   "new",
	Short: "Enter account/invite menu",
}

var rm = &cobra.Command{
	Use:   "rm [uuid]",
	Short: "Delete an invitation/user account",
	Args:  cobra.MinimumNArgs(1),
}

// --account (uuid)
var show = &cobra.Command{
	Use:   "show",
	Short: "Display information about accounts/invite links",
	Args:  cobra.MaximumNArgs(1),
}

// --off
var track = &cobra.Command{
	Use:   "track [uuid]",
	Short: "Toggles activity tracking for a given user account",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("Requires a UUID")
		}

		uid, err := uuid.Parse(args[0])

		if err != nil {
			return fmt.Errorf("Invalid uuid '%s': %s", uid.String(), err)
		}

		return nil
	},
}

var genInviteURL = &cobra.Command{
	Use:   "gen-invite-url [token]",
	Short: "Generates an invite URL string given an invitation token",
	Args:  cobra.MinimumNArgs(1),
}

func processCommand(cmd string, aliases *map[string]string, root *cobra.Command) error {
	// Split string by whitespace
	args := strings.Fields(cmd)

	// Ignore empty commands
	if len(args) == 0 {
		return nil
	}

	// Expand command aliases
	if aliases != nil {
		expanded, exists := (*aliases)[args[0]]

		// Alias replacement found
		if exists {
			newArgs := strings.Fields(expanded)

			// Replace the shortcut at pos 1 with
			// split version of the expanded command + arguments without the cmd
			args = append(newArgs, args[1:]...)
		}
	}

	if root == nil {
		return errors.New("Root command is nil")
	}

	root.SetArgs(args)

	// Parse cli arguments
	return root.Execute()
}
