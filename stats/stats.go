package stats

import "time"
import "sync/atomic" 

type Stats struct {
	// Number of http requests handled
	// on the /auth path
	AuthRequests int64

	// Number of http requests handled
	// on the /invite path
	InviteRequests int64

	// Number of successfully authenticated users
	AuthUsers int64

	// Number of failed user authentication attempts
	AuthFailed int64

	// Numbers of links that have been successfully activated
	ActivatedLinks int64

	// Number of failed activation links(when someone tries to activate an invite link that expired
	FailedLinkActivation int64

	// Numbers of connections made to the management console
	ConsoleConnections int64

	// Number of failed login attepts on the management console
	ConsoleLoginFail int64

	// Number of successful logins on the management console
	ConsoleLogin int64

	// Timestamp of last successful login on the management console
	ConsoleLastLogin time.Time
}

// Helper functions.
// Yes you can update the data directly, but is better to use a wraper function
// in order to avoid future mistakes and make the code more readable

func (s *Stats) NewAuthRequest() {
	// Stats is used as a global variable. It can get overwritten by mistake by any code path
	// This check refuses any operations on invalid structures
	if s == nil {
		return
	}

	atomic.AddInt64(&s.AuthRequests, 1)
}

func (s *Stats) NewInviteRequest() {
	if s == nil {
		return
	}

	atomic.AddInt64(&s.InviteRequests, 1)
}

// Update the number of successfully authenticated users
func (s *Stats) NewAuthUser() {
	if s == nil {
		return
	}

	atomic.AddInt64(&s.AuthUsers, 1)
}

func (s *Stats) AuthenticationFailed() {
	if s == nil {
		return
	}

	atomic.AddInt64(&s.AuthFailed, 1)
}

func (s *Stats) NewActivatedLink() {
	if s == nil {
		return
	}

	atomic.AddInt64(&s.ActivatedLinks, 1)
}

func (s *Stats) LinkActivationFailed() {
	if s == nil {
		return
	}

	atomic.AddInt64(&s.FailedLinkActivation, 1)
}

func (s *Stats) NewConsoleConnections() {
	if s == nil {
		return
	}

	atomic.AddInt64(&s.ConsoleConnections, 1)
}

func (s *Stats) ConsoleFailedLogin() {
	if s == nil {
		return
	}

	atomic.AddInt64(&s.ConsoleLoginFail, 1)
}

func (s *Stats) NewConsoleLogin() {
	if s == nil {
		return
	}

	atomic.AddInt64(&s.ConsoleLogin, 1)
}

func (s *Stats) NewLogin() {
	if s == nil {
		return
	}

	s.ConsoleLastLogin = time.Now()
}
