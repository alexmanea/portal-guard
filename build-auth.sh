#!/bin/sh
name="portal.exe"

rm -r build

# Install program dependencies
echo "[+] Installing dependencies..."
go get github.com/boltdb/bolt
go get github.com/google/uuid
go get github.com/gorilla/securecookie
go get github.com/julienschmidt/httprouter
go get github.com/spf13/cobra
go get github.com/gliderlabs/ssh
go get golang.org/x/crypto/ssh
go get golang.org/x/crypto/ssh/terminal

echo "[+] Building program..."

if [ ! -d build ]; then
	mkdir build
fi

# Strip binary from symbols and store it in the build folder
go build -o "build/$name" main.go && echo "[+] Success!"

# Creating an encrytped file to store the database keys
echo "[+] Securing database keys..."

genCmd="openssl rand -base64"

# Generate random keys
echo "[+] Generating random encryption keys for database..."

dbk=`$genCmd 32`
dbiv=`$genCmd 16`
ckey=`$genCmd 32`
cauthkey=`$genCmd 32`

printf "%s,%s,%s,%s\n" "$dbk" "$dbiv" "$ckey" "$cauthkey" > build/portal.keys
